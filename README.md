# ReDemocratising  AI

Repository for project ideas aimed at the re-democratisation of ML/AI methods. 

## Table of contents
1. [Project Overview](#project-overview)
2. [People and Resources](#people-and-resources)
3. [Task 1](#task-1)
4. [Task 2](#task-2)

## Project Overview

## People and Resources

## Task 1
### **Demographic survey within ML/AI**

### Description

Bulk of the recent developments in ML/AI have been steered by the Global North (with an exception of China). To obtain an understanding the demographic skew, this task aims to survey the domain of ML/AI and provide some concrete statistics. This can be either using only academic publishing as a marker or using ML/AI based companies. As the first step, using open data drawn from academic publications could serve as an easy-to-access data source.  

## Task 2
### **Low-resource ML/AI**

### Description

The material costs of performing large scale ML/AI is exorbitant. Developing these methods on old/low-resource hardware can be immensely useful to bridge the divide in bringing the Global South, meaningfully into the ML/AI research/industry. 

Depending on the technical interests of the candidates, this project could be something highly technical to something more mathematical. Some suggested topics for projects along these lines are listed below:

1. Porting ML/AI models to low resource embedded devices. For instance, Arduino
2. Active Learning: Modelling useful data points for training models
3. Extreme quantisation: Enabling low precision inference at INT1/INT2 precision
4. Physics informed ML: Incorporating model based knowledge as physical constraints into DL models, to reduce data/compute requirements


## Task 3
### **Using ML/AI for Sustainability Applications**

India offers a unique opportunity for the use of advanced ML/AI models in the battle against climate change. Creative applications at the intersection of ML and sustainability with a focus on Indian conditions will be useful.    
